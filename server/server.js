const express = require('express');
const app = express();
const { resolve } = require('path');
const bodyParser = require('body-parser');
const https = require('https');
const fs = require('fs');
const request = require('request');
//const request = require('request');
const queryString = require('query-string');
//var enforce = require('express-sslify');
// Replace if using a different env file or config
require('dotenv').config({ path: './.env' });

if (
  !process.env.STRIPE_SECRET_KEY ||
  !process.env.STRIPE_PUBLISHABLE_KEY ||
  !process.env.BASIC ||
  !process.env.PREMIUM ||
  !process.env.STATIC_DIR
) {
  console.log(
    'The .env file is not configured. Follow the instructions in the readme to configure the .env file. https://github.com/stripe-samples/subscription-use-cases'
  );
  console.log('');
  process.env.STRIPE_SECRET_KEY
    ? ''
    : console.log('Add STRIPE_SECRET_KEY to your .env file.');

  process.env.STRIPE_PUBLISHABLE_KEY
    ? ''
    : console.log('Add STRIPE_PUBLISHABLE_KEY to your .env file.');

  process.env.BASIC
    ? ''
    : console.log(
        'Add BASIC priceID to your .env file. See repo readme for setup instructions.'
      );

  process.env.PREMIUM
    ? ''
    : console.log(
        'Add PREMIUM priceID to your .env file. See repo readme for setup instructions.'
      );

  process.env.STATIC_DIR
    ? ''
    : console.log(
        'Add STATIC_DIR to your .env file. Check .env.example in the root folder for an example'
      );

  process.exit();
}

const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

app.use(express.static(process.env.STATIC_DIR));
// Use JSON parser for all non-webhook routes.
app.use((req, res, next) => {
  if (req.originalUrl === '/stripe-webhook') {
    next();
  } else {
    bodyParser.json()(req, res, next);
  }
});

app.get('/', (req, res) => {
  const path = resolve(process.env.STATIC_DIR + '/index.html');
  res.sendFile(path);
});

app.get('/config', async (req, res) => {
  res.send({
    publishableKey: process.env.STRIPE_PUBLISHABLE_KEY,
  });
});

app.post('/create-customer', async (req, res) => {
    try{
        const customers = await stripe.customers.list();
  var customer_i = null;
  for(var i=0;i<customers.data.length;i++){
    
    if( JSON.stringify(req.body.email) === JSON.stringify(customers.data[i].email)){
         console.log(customers.data[i].email);
         customer_i = customers.data[i].id;
         break;
    }
  }
  // Create a new customer object
  if(customer_i){
      const customer = await stripe.customers.retrieve(
   customer_i);
   console.log({"customer exists":customer});
   
   const subscriptions = await stripe.subscriptions.list();
  var subscriptionx  = [];
  var payment_methodx = null;
  var price_id = null;
  for(var i=0;i<subscriptions.data.length;i++){
  console.log({'customer_i':customer_i}, {'subscriptions.data[i].customer':subscriptions.data[i].customer})
    if( JSON.stringify(customer_i) === JSON.stringify(subscriptions.data[i].customer)){
         console.log(subscriptions.data[i], "subscriptions.data[i]");
         if(subscriptions.data[i].status=='trialing'){
             subscriptionx = [subscriptions.data[i]];
         }
         else{
             console.log({'subscriptions.data[i].latest_invoice':subscriptions.data[i].latest_invoice});
             const invoice = await stripe.invoices.retrieve(
                  subscriptions.data[i].latest_invoice
                );
             console.log({'subscriptions.data[i].latest_invoice':subscriptions.data[i].latest_invoice});
             const paymentIntent = await stripe.paymentIntents.retrieve(
                  invoice.payment_intent
                );
             console.log({'invoice.payment_intent':invoice.payment_intent});
             const payment_method_ = paymentIntent.payment_method;
             console.log({'payment_method_':payment_method_});
             
             subscriptionx = [subscriptions.data[i]];
             payment_methodx = payment_method_;
             price_id = invoice.lines.data[0].price.id;
         }
         
         break;
    }
  }
      var content_html= `<button id="speak" style="width:100%;">Play With Sound 🔊</button>
        <button id="stop" style="width:100%;">Stop Voice ⏹</button>
        <div id="summernote"  style="background-color:white;height=100%">This application predicts what you are going to write next. So you need to start with something. This is fine too.</div>
         
        <button style="width:100%;position: fixed;
        bottom: 0;
        right: 0;" id="predict" type="button" class="btn btn-primary">PREDICT</button>
        `;
      console.log(subscriptionx);
      res.send({ price_idx: price_id,customer,subscription:subscriptionx,  'exists':true,payment_method:payment_methodx, 'content':content_html});
      console.log("customer exits");
      
  }
  else{
      console.log("customer DNE");
      const customer = await stripe.customers.create({
        email: req.body.email,
      });

      // save the customer.id as stripeCustomerId
      // in your database.
      
      res.send({ customer , "exists":false});
  }
}catch(error){
    
    console.log("############################## //create-customer"+error);

    if((""+error).includes("GET /payment_intents/{id}"))
        try{
            
            
            console.log("customer DNE");
      const customer = await stripe.customers.create({
        email: req.body.email,
      });

      // save the customer.id as stripeCustomerId
      // in your database.
      /*
        try{
            
            
            
            const subscriptions = await stripe.subscriptions.list();
  var subscription = null;
  for(var i=0;i<subscriptions.data.length;i++){
    
    if(  customer.id === JSON.stringify(subscriptions.data[i].customer)){
         console.log(subscriptions.data[i]);
         subscription = subscriptions.data[i];
         const deleted = await stripe.subscriptions.del(
  subscription.id
);
    console.log("deleted:", deleted)
    }
  }
            
            
        }catch(error){console.log(error);}
      */
      
            res.send({ "customer":customer , "exists":"payment_error"});
            
            
            }catch(error){console.log("############################## //create-customer error 2 "+error);}
    else
        try{res.send({ "customer":null , "exists":"email_error"});}catch(error){console.log("############################## //create-customer error 2 "+error);}

}
  
});

app.get('/get-customer', async (req, res) => {
    try{
  const customers = await stripe.customers.list();
  var is_customer = false;
  for(var i=0;i<customers.data.length;i++){
    
    if( JSON.stringify(req.query.email) === JSON.stringify(customers.data[i].email)){
         console.log(customers.data[i].email);
         is_customer = true;
    }
  }
  
  res.send({false_:req.query.email} );
}catch(error){console.log("############################## //get-customer"+error);}
  
});

app.get('/get-app', async (req, res) => {
    try{
/*request('https://13.89.233.69:8000/?'+req.query, (err, res, body) => {
  if (err) { return console.log(err); }
  console.log(res, res.data);
  return res.data;
});
*/

//  req.query
https.get({host:'13.89.233.69', path:'/?'+queryString.stringify(req.query), port:'8000', rejectUnauthorized: false}, (resp) => {
res.setHeader('Content-Type', 'text/plain');
  console.log(resp);
    let data = '';

  // A chunk of data has been recieved.
  resp.on('data', (chunk) => {
    data += chunk;
  });

  // The whole response has been received. Print out the result.
  resp.on('end', () => {
    console.log(data);
    res.end(data);
  });

//res.write(data);
//res.end();
}).on("error", (err) => {
  console.log("Error: " + err.message);
});

}catch(error){console.log("############################## //get-customer"+error);}

});
app.get('/get-subscription', async (req, res) => {
    try{
  const subscriptions = await stripe.subscriptions.list();
  var subscription = null;
  for(var i=0;i<subscriptions.data.length;i++){
    
    if( JSON.stringify(req.query.cid) === JSON.stringify(subscriptions.data[i].customer)){
         console.log(subscriptions.data[i]);
         subscription = subscriptions.data[i];
         console.log({'subscription.latest_invoice':subscription.latest_invoice});
         const invoice = await stripe.invoices.retrieve(
              subscription.latest_invoice
            );
         console.log({'subscription.latest_invoice':subscription.latest_invoice});
         const paymentIntent = await stripe.paymentIntents.retrieve(
              invoice.payment_intent
            );
         console.log({'invoice.payment_intent':invoice.payment_intent});
         const payment_method_ = paymentIntent.payment_method;
         console.log({'payment_method_':payment_method_});
         subscription.push({payment_method:payment_method_});
         break;
    }
  }
  if(subscription)
      res.send({true_:subscription} );
  else
      res.send({false_:subscription} );
  
}catch(error){console.log("############################## //get-customer"+error);}
  
});

app.post('/create-subscription', async (req, res) => {
    const TRIAL_DAYS = 0;
  // Set the default payment method on the customer
  try {
    await stripe.paymentMethods.attach(req.body.paymentMethodId, {
      customer: req.body.customerId,
    });
  } catch (error) {
    return res.status('402').send({ error: { message: error.message } });
  }
try{
  let updateCustomerDefaultPaymentMethod = await stripe.customers.update(
    req.body.customerId,
    {
      invoice_settings: {
        default_payment_method: req.body.paymentMethodId,
      },
    }
  );
    
  // Create the subscription
  if(TRIAL_DAYS>0){
  const subscription = await stripe.subscriptions.create({
    customer: req.body.customerId,
    items: [{ plan: 'price_1HgcIhCImIpiRbxKRV4jJKZo' }],
    expand: ['latest_invoice.payment_intent'],
    trial_period_days:TRIAL_DAYS
  });
  res.send(subscription);
  }else{
      const subscription = await stripe.subscriptions.create({
    customer: req.body.customerId,
    items: [{ plan: 'price_1HgcIhCImIpiRbxKRV4jJKZo' }],
    expand: ['latest_invoice.payment_intent']
  });
  res.send(subscription);
  }
}catch(error){console.log("############################## /create-subscription"+error);}
  
});

app.post('/retry-invoice', async (req, res) => {
  // Set the default payment method on the customer
  console.log("###########################################################");
  try {
    await stripe.paymentMethods.attach(req.body.paymentMethodId, {
      customer: req.body.customerId,
    });
    await stripe.customers.update(req.body.customerId, {
      invoice_settings: {
        default_payment_method: req.body.paymentMethodId,
      },
    });
  } catch (error) {
    // in case card_decline error
    return res
      .status('402')
      .send({ result: { error: { message: error.message } } });
  }
  try {
  const invoice = await stripe.invoices.retrieve(req.body.invoiceId, {
    expand: ['payment_intent'],
  });
  res.send(invoice);
  } catch (error) {
    // in case card_decline error
    return res
      .status('402')
      .send({ result: { error: { message: error.message } } });
  }
});

app.post('/retrieve-upcoming-invoice', async (req, res) => {
    try{
  const subscription = await stripe.subscriptions.retrieve(
    req.body.subscriptionId
  );

  const invoice = await stripe.invoices.retrieveUpcoming({
    subscription_prorate: true,
    customer: req.body.customerId,
    subscription: req.body.subscriptionId,
    subscription_items: [
      {
        id: subscription.items.data[0].id,
        deleted: true,
      },
      {
        price: process.env[req.body.newPriceId],
        deleted: false,
      },
    ],
  });
  res.send(invoice);
  }catch(error){console.log("############################## /retrieve-upcoming-invoice "+error);}
  
});

app.post('/cancel-subscription', async (req, res) => {
  // Delete the subscription
  try{
      const deletedSubscription = await stripe.subscriptions.del(
    req.body.subscriptionId
  );
  res.send(deletedSubscription);
  
  }catch(error){console.log(error);}
});

app.post('/update-subscription', async (req, res) => {
    try{
  const subscription = await stripe.subscriptions.retrieve(
    req.body.subscriptionId
  );
  
  const updatedSubscription = await stripe.subscriptions.update(
    req.body.subscriptionId,
    {
      cancel_at_period_end: false,
      items: [
        {
          id: subscription.items.data[0].id,
          price: process.env[req.body.newPriceId],
        },
      ],
    }
  );
  res.send(updatedSubscription);
}catch(error){console.log("############################## /update-subscription "+error);}
  
});

app.post('/retrieve-customer-payment-method', async (req, res) => {
   
  const paymentMethod = await stripe.paymentMethods.retrieve(
    req.body.paymentMethodId
  );res.send(paymentMethod);
    
  
});
// Webhook handler for asynchronous events.
app.post(
  '/stripe-webhook',
  bodyParser.raw({ type: 'application/json' }),
  async (req, res) => {
    // Retrieve the event by verifying the signature using the raw body and secret.
    let event;

    try {
      event = stripe.webhooks.constructEvent(
        req.body,
        req.header('Stripe-Signature'),
        process.env.STRIPE_WEBHOOK_SECRET
      );
    } catch (err) {
      console.log(err);
      console.log(`⚠️  Webhook signature verification failed.`);
      console.log(
        `⚠️  Check the env file and enter the correct webhook secret.`
      );
      return res.sendStatus(400);
    }
    // Extract the object from the event.
    const dataObject = event.data.object;

    // Handle the event
    // Review important events for Billing webhooks
    // https://stripe.com/docs/billing/webhooks
    // Remove comment to see the various objects sent for this sample
    switch (event.type) {
      case 'invoice.paid':
        // Used to provision services after the trial has ended.
        // The status of the invoice will show up as paid. Store the status in your
        // database to reference when a user accesses your service to avoid hitting rate limits.
        break;
      case 'invoice.payment_failed':
        // If the payment fails or the customer does not have a valid payment method,
        //  an invoice.payment_failed event is sent, the subscription becomes past_due.
        // Use this webhook to notify your user that their payment has
        // failed and to retrieve new card details.
        break;
      case 'invoice.finalized':
        // If you want to manually send out invoices to your customers
        // or store them locally to reference to avoid hitting Stripe rate limits.
        break;
      case 'customer.subscription.deleted':
        if (event.request != null) {
          // handle a subscription cancelled by your request
          // from above.
        } else {
          // handle subscription cancelled automatically based
          // upon your subscription settings.
        }
        break;
      case 'customer.subscription.trial_will_end':
        // Send notification to your user that the trial will end
        break;
      default:
      // Unexpected event type
    }
    res.sendStatus(200);
  }
);
// set up a route to redirect http to https
/*const baseUrl = "idea-pour.herokuapp.com";
console.log(baseUrl);
https.get(baseUrl, function(req, res) {  
    res.redirect('https://' + req.headers.host + req.url);

    // Or, if you don't want to automatically detect the domain name from the request header, you can hard code it:
    // res.redirect('https://example.com' + req.url);
})
*/
// have it listen on 8080
//https.listen(process.env.PORT || 4242);
// we will pass our 'app' to 'https' server
/*https.createServer(app)
.listen(process.env.PORT || 4242, () => console.log(`Node server listening on port ${process.env.PORT || 4242}!`));
*/

 
// Use enforce.HTTPS({ trustProtoHeader: true }) in case you are behind
// a load balancer (e.g. Heroku). See further comments below
/*app.use((req, res, next) => {
  if (req.header('x-forwarded-proto') !== 'https') {
    res.redirect(`https://${req.header('host')}${req.url}`)
  } else {
    next();
  }
});*/
app.listen(process.env.PORT || 4242, () => console.log(`Node server listening on port ${process.env.PORT || 4242}!`));
/*
var http = require('http');
var server = http.createServer((req, res) => {
  res.writeHead(301,{Location: `https://${req.headers.host}${req.url}`});
  res.end();
});

server.listen(80);
*/